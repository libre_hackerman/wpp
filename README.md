# wpp

WPP is a wallpaper changer written in Python 3 that relies in Feh. Oriented to WM users that want to set a rotation of wallpapers.

The syntax of the special options file is the name of the picture, a space, a colon, a space and the options to pass to Feh. Example:
```
picture1.png : --bg-fill --geometry +0-30
picture2.png : --bg-max
pictureN.jpg : --option1 --option2 --optionN
```


If you have a WM config file and you want to restart WPP every time the config file is executed
you can put this code:

```
[ -f /tmp/wpp_pid ] && kill $(cat /tmp/wpp_pid)
wpp -i &
echo $! > /tmp/wpp_pid
```
